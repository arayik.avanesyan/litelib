FROM amazoncorretto:17-alpine-jdk
COPY ./api/target/liltelib.jar /usr/src/api/
WORKDIR /usr/src/api
EXPOSE 8080
CMD ["java",  "-jar", "liltelib.jar"]
