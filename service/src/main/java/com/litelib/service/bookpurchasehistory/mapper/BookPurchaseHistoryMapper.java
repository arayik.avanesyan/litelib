package com.litelib.service.bookpurchasehistory.mapper;

import com.litelib.persistence.purchasehistory.BookPurchaseHistory;
import com.litelib.service.author.mapper.AuthorMapper;
import com.litelib.service.bookpurchasehistory.dto.BookPurchaseHistoryDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = AuthorMapper.class)
public interface BookPurchaseHistoryMapper {

    @Mapping(target = "bookId", source = "purchaseHistory.book.id")
    @Mapping(target = "title", source = "purchaseHistory.book.title")
    @Mapping(target = "authors", source = "purchaseHistory.book.authors")
    BookPurchaseHistoryDto mapEntityToDto(final BookPurchaseHistory purchaseHistory);

    List<BookPurchaseHistoryDto> mapEntitiesToDtoList(final List<BookPurchaseHistory> purchaseHistory);

}
