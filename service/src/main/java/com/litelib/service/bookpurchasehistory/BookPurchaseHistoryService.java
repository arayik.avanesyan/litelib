package com.litelib.service.bookpurchasehistory;

import com.litelib.persistence.book.Book;
import com.litelib.service.bookpurchasehistory.dto.BookPurchaseHistoryDto;

import java.util.List;

public interface BookPurchaseHistoryService {

    List<BookPurchaseHistoryDto> getALl();

    void create(final Book book, final Long quantity);

}
