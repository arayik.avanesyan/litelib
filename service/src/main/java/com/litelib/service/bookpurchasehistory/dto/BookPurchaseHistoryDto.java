package com.litelib.service.bookpurchasehistory.dto;

import com.litelib.service.booktoauthor.dto.AuthorDto;
import lombok.Data;

import java.util.Set;

@Data
public class BookPurchaseHistoryDto {

    private Long bookId;

    private String title;

    private Set<AuthorDto> authors;

    private Long quantity;

}
