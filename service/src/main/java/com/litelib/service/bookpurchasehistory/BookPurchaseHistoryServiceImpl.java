package com.litelib.service.bookpurchasehistory;

import com.litelib.persistence.book.Book;
import com.litelib.persistence.purchasehistory.BookPurchaseHistoryRepository;
import com.litelib.persistence.purchasehistory.BookPurchaseHistory;
import com.litelib.service.bookpurchasehistory.dto.BookPurchaseHistoryDto;
import com.litelib.service.bookpurchasehistory.mapper.BookPurchaseHistoryMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Log4j2
@RequiredArgsConstructor
@Service
public class BookPurchaseHistoryServiceImpl implements BookPurchaseHistoryService {

    private final BookPurchaseHistoryRepository bookPurchaseHistoryRepository;

    private final BookPurchaseHistoryMapper historyMapper;

    @Transactional
    @Override
    public List<BookPurchaseHistoryDto> getALl() {
        log.info("Loading books purchase history");
        final List<BookPurchaseHistory> historyList = bookPurchaseHistoryRepository.findAll();
        return historyMapper.mapEntitiesToDtoList(historyList);
    }

    @Transactional
    @Override
    public void create(final Book book, final Long quantity) {
        log.info("Saving book purchase history");
        final BookPurchaseHistory bookPurchaseHistory = new BookPurchaseHistory();
        bookPurchaseHistory.setBook(book);
        bookPurchaseHistory.setQuantity(quantity);
        bookPurchaseHistoryRepository.save(bookPurchaseHistory);
    }

}
