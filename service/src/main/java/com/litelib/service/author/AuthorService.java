package com.litelib.service.author;

import com.litelib.persistence.author.Author;

public interface AuthorService {

    Author createAndGet(final String authorName);

}
