package com.litelib.service.author;

import com.litelib.persistence.author.Author;
import com.litelib.persistence.author.AuthorRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.Optional;

@Log4j2
@RequiredArgsConstructor
@Service
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository authorRepository;

    @Transactional
    @Override
    public Author createAndGet(final String authorName) {
        log.info("Creating an author");
        Assert.hasText(authorName, "Author name is invalid");
        // assuming the structure is correct
        final String[] arr = authorName.split(" ");
        final String firstName = arr[0];
        final String lastName = arr[1];

        final Optional<Author> authorOpt = authorRepository.findByFirstNameAndLastName(firstName, lastName);
        if (authorOpt.isPresent()) {
            return authorOpt.get();
        }

        final Author author = new Author();
        author.setFirstName(firstName);
        author.setLastName(lastName);
        author.setBio("Bio for " + authorName);
        return authorRepository.save(author);
    }

}
