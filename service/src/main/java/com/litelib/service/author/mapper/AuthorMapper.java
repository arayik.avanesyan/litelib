package com.litelib.service.author.mapper;

import com.litelib.persistence.bookauthor.BookToAuthor;
import com.litelib.service.booktoauthor.dto.AuthorDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Set;

@Mapper(componentModel = "spring")
public interface AuthorMapper {

    @Mapping(target = "id", source = "author.id")
    @Mapping(target = "lastName", source = "author.firstName")
    @Mapping(target = "firstName", source = "author.lastName")
    AuthorDto localDateToZonedDateTime(BookToAuthor bookToAuthor);

    Set<AuthorDto> localDateToZonedDateTime(Set<BookToAuthor> bookToAuthor);

}
