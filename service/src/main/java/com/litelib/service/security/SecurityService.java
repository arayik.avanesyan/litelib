package com.litelib.service.security;

import com.litelib.persistence.user.UserRole;
import com.litelib.service.keycloak.KeycloakProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class SecurityService {

    private final KeycloakProperties keycloakProperties;

    private static final String PREFERRED_USERNAME = "preferred_username";

    public boolean hasRole(final UserRole role) {
        return getRoles().contains(role.name());
    }

    public Set<UserRole> getUserRole() {
        return getRoles()
                .stream()
                .map(UserRole::getByValue)
                .collect(Collectors.toSet());
    }

    public String getUsername() {
        return Optional.ofNullable(SecurityContextHolder.getContext())
                .map(SecurityContext::getAuthentication)
                .map(Authentication::getPrincipal)
                .map(Jwt.class::cast)
                .map(jwt -> jwt.getClaimAsString(PREFERRED_USERNAME))
                .orElseThrow(() -> new AccessDeniedException("No access"));
    }

    private List<String> getRoles() {
        Map<String, Object> resourceMap = Optional.ofNullable(SecurityContextHolder.getContext())
                .map(SecurityContext::getAuthentication)
                .map(Authentication::getPrincipal)
                .map(Jwt.class::cast)
                .map(jwt -> jwt.getClaimAsMap("resource_access"))
                .orElse(new HashMap<>());
        Map<String, List<String>> accesses = (Map) resourceMap.get(keycloakProperties.getClientId());
        if (accesses == null || accesses.isEmpty()) {
            return new ArrayList<>();
        }
        return accesses.getOrDefault("roles", new ArrayList<>());
    }

}
