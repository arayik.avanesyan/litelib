package com.litelib.service.csv;

import org.apache.commons.csv.CSVRecord;

public interface CsvRecordConverter<T> {

    T convert(final CSVRecord csvRecord);

}
