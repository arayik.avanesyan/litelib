package com.litelib.service.csv;

import com.litelib.service.user.dto.UserCreateDto;
import lombok.extern.java.Log;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.csv.CSVRecord;

@Log4j2
public class CsvUserRecordConverterImpl implements CsvRecordConverter<UserCreateDto> {

    @Override
    public UserCreateDto convert(CSVRecord csvRecord) {
        log.debug("Converting CSVRecord");
        // assuming all users name field will contain first name and last name
        final String[] name = csvRecord.get("name").split(" ");
        UserCreateDto userCreateDto = new UserCreateDto();
        userCreateDto.setFirstName(name[0]);
        userCreateDto.setLastName(name[1]);
        userCreateDto.setUsername(csvRecord.get("email"));
        userCreateDto.setPassword(csvRecord.get("password"));
        userCreateDto.setEmail(csvRecord.get("email"));
        return userCreateDto;
    }

}
