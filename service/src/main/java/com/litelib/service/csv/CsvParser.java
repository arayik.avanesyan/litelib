package com.litelib.service.csv;

import com.litelib.service.exceptions.CsvParsingRuntimeException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.apache.commons.csv.CSVFormat.EXCEL;

@Log4j2
@Component
public class CsvParser {

    public <T> List<T> parse(final MultipartFile file, final CsvRecordConverter<T> recordConverter) {
        Assert.notNull(file, "File should not be null");
        Assert.notNull(recordConverter, "Record converter is required");
        log.info("Starting csv file parsing ...");
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(file.getInputStream(), UTF_8))) {
            try (CSVParser csvParser = new CSVParser(fileReader, EXCEL.withHeader())) {
                Iterable<CSVRecord> csvRecords = csvParser.getRecords();

                List<T> users = new ArrayList<>();
                for (CSVRecord csvRecord : csvRecords) {
                    users.add(recordConverter.convert(csvRecord));
                }
                return users;
            }
        } catch (IOException e) {
            throw new CsvParsingRuntimeException(e, "Csv parsing exception");
        }
    }

}
