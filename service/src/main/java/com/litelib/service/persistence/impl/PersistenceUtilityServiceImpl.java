package com.litelib.service.persistence.impl;

import com.litelib.service.persistence.PersistenceUtilityService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;


@Log4j2
@Service
public class PersistenceUtilityServiceImpl implements PersistenceUtilityService {

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public void runInNewTransaction(final Runnable runnable) {
        Assert.notNull(runnable, "Runnable should not be null when running in new transaction");
        log.debug("Running task in new transaction");
        runnable.run();
    }
}
