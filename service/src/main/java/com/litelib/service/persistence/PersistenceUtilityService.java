package com.litelib.service.persistence;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


public interface PersistenceUtilityService {

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    void runInNewTransaction(Runnable runnable);

}
