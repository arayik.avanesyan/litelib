package com.litelib.service.user;

import com.litelib.persistence.user.User;
import com.litelib.persistence.user.UserType;
import com.litelib.service.user.dto.UserCreateDto;
import com.litelib.service.user.dto.UserDto;
import com.litelib.service.user.dto.UserUpdateDto;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface UserService {

    List<UserDto> getAll();

    Long create(final UserCreateDto createDto, UserType type, final boolean tempPassword);

    void update(final Long id, final UserUpdateDto userUpdateDto);

    void delete(final Long id);

    void importUsers(final MultipartFile file);

    User getUser();
}
