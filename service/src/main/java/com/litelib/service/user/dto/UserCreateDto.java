package com.litelib.service.user.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class UserCreateDto {

    @Size(max = 50, message = "First name length is exceeded max size (50)")
    private String firstName;

    @Size(max = 50, message = "First name length is exceeded max size (50)")
    private String lastName;

    @Email
    @NotNull
    private String email;

    @NotBlank
    @Size(max = 50, message = "Username length is exceeded max size (50)")
    private String username;

    @NotBlank
    private String password;

}
