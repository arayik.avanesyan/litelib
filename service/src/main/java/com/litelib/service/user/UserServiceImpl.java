package com.litelib.service.user;

import com.litelib.persistence.user.User;
import com.litelib.persistence.user.UserRepository;
import com.litelib.persistence.user.UserRole;
import com.litelib.persistence.user.UserType;
import com.litelib.service.csv.CsvParser;
import com.litelib.service.csv.CsvUserRecordConverterImpl;
import com.litelib.service.exceptions.DuplicateEntityForConditionException;
import com.litelib.service.exceptions.ServiceRuntimeException;
import com.litelib.service.keycloak.KeycloakService;
import com.litelib.service.keycloak.dto.KeycloakUserCreateDto;
import com.litelib.service.keycloak.dto.KeycloakUserUpdateDto;
import com.litelib.service.persistence.PersistenceUtilityService;
import com.litelib.service.security.SecurityService;
import com.litelib.service.user.dto.UserCreateDto;
import com.litelib.service.user.dto.UserDto;
import com.litelib.service.user.dto.UserUpdateDto;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;

import javax.ws.rs.NotAuthorizedException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final KeycloakService keycloakService;

    private final SecurityService securityService;

    private final PersistenceUtilityService persistenceUtilityService;

    private final CsvParser csvParser;

    @Value("${user.import.csv.max-size-bytes}")
    private Long maxCsvSize;

    @Value("${user.import.default-password}")
    private String defaultPassword;

    @Override
    public List<UserDto> getAll() {
        UserType userType;
        if (securityService.getUserRole().contains(UserRole.SUPER_ADMIN)) {
            userType = UserType.ADMIN;
        } else {
            userType = UserType.USER;
        }
        List<User> users = userRepository.findAllByType(userType);
        return users.stream()
                .map(this::mapEntityToDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public Long create(final UserCreateDto createDto, final UserType type, final boolean tempPassword) {
        log.info("Creating a user");
        validateUserCreateDto(createDto);

        // Creating the user in sso
        log.info("Creating a user for sso");
        final KeycloakUserCreateDto keycloakUserDto = createKeycloakUserCreateDto(createDto);
        keycloakUserDto.setTempPassword(tempPassword);
        keycloakUserDto.setGroup(type.getGroup());
        final var ssoId = keycloakService.createUser(keycloakUserDto);
        log.info("User was created with ssoId: {}", ssoId);

        // Saving the user
        final User user = mapCreateDtoToUser(createDto);
        user.setSsoId(ssoId);
        user.setType(type);

        return userRepository.save(user).getId();
    }

    @Transactional
    @Override
    public void update(final Long id, final UserUpdateDto userUpdateDto) {
        log.info("Updating user {}", id);
        final User user = getById(id);
        validateUserUpdateDto(user.getType());
        user.setFirstName(userUpdateDto.getFirstName());
        user.setLastName(userUpdateDto.getLastName());

        // Updating user in keycloak
        log.info("Updating user in sso");
        final KeycloakUserUpdateDto keycloakUserDto = createKeycloakUserUpdateDto(userUpdateDto);
        keycloakUserDto.setSsoId(user.getSsoId());
        keycloakUserDto.setEmail(user.getEmail());
        keycloakService.updateUser(keycloakUserDto);
        log.info("User was updated");

    }

    @Transactional
    @Override
    public void delete(final Long id) {
        log.info("Deleting user by id {}", id);
        Assert.notNull(id, "User id is null");
        final User user = getById(id);
        validateUserDelete(user.getType());
        user.markRemoved();
        keycloakService.disableUser(user.getEmail(), user.getSsoId());
    }

    @Transactional
    @Override
    public void importUsers(final MultipartFile file) {
        log.info("Importing users");
        validateCsvFile(file);

        log.info("Parsing the file ...");
        final List<UserCreateDto> users = csvParser.parse(file, new CsvUserRecordConverterImpl());
        final UserType type = securityService.hasRole(UserRole.ADMIN) ? UserType.USER : UserType.ADMIN;
        log.info("Total {} users ({}) were extracted", users.size(), type);

        log.info("Saving extracted users");
        final List<String> nonImportedUsers = new ArrayList<>();
        users.forEach(createDto -> {
            try {
                //ensures that the failure to create one user (e.g. duplicate email or username) does not affect the creation of others
                persistenceUtilityService.runInNewTransaction(() -> create(createDto, type, false));
            } catch (final DuplicateEntityForConditionException e) {
                nonImportedUsers.add(createDto.getEmail());
            } catch (final NotAuthorizedException | ServiceRuntimeException e) {
                log.error("Not able not connect to Keycloak / unauthorized access");
                throw new ServiceRuntimeException(e.getMessage());
            }
        });
        if (!nonImportedUsers.isEmpty()) {
            log.warn("There are {} non-imported users", nonImportedUsers.size());
            throw new DuplicateEntityForConditionException(nonImportedUsers.toString(), "email/username");
        }
    }

    @Override
    public User getUser() {
        final String username = securityService.getUsername();
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException("User was not found by email"));
    }

    private User getById(final Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("User was not found"));
    }

    // region mappers
    private User mapCreateDtoToUser(final UserCreateDto createDto) {
        final User user = new User();
        user.setFirstName(createDto.getFirstName());
        user.setLastName(createDto.getLastName());
        user.setEmail(createDto.getEmail());
        user.setUsername(createDto.getUsername());
        return user;
    }

    private UserDto mapEntityToDto(final User user) {
        final UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEmail(user.getEmail());
        userDto.setUsername(user.getUsername());
        return userDto;
    }

    private KeycloakUserCreateDto createKeycloakUserCreateDto(final UserCreateDto dto) {
        final KeycloakUserCreateDto keycloakUserCreateDto = new KeycloakUserCreateDto();
        keycloakUserCreateDto.setUsername(dto.getUsername());
        keycloakUserCreateDto.setFirstName(dto.getFirstName());
        keycloakUserCreateDto.setEmail(dto.getEmail());
        keycloakUserCreateDto.setLastName(dto.getLastName());
        keycloakUserCreateDto.setEnabled(true);
        keycloakUserCreateDto.setPassword(dto.getPassword() != null ? dto.getPassword() : defaultPassword);
        return keycloakUserCreateDto;
    }

    private KeycloakUserUpdateDto createKeycloakUserUpdateDto(final UserUpdateDto dto) {
        final KeycloakUserUpdateDto keycloakUserCreateDto = new KeycloakUserUpdateDto();
        keycloakUserCreateDto.setFirstName(dto.getFirstName());
        keycloakUserCreateDto.setLastName(dto.getLastName());
        return keycloakUserCreateDto;
    }
    // endregion

    // region validation
    private void validateCsvFile(final MultipartFile file) {
        log.info("Validation csv file");
        long size = file.getSize();
        Assert.isTrue(size <= maxCsvSize, "Csv max size is exceeded");
    }

    private void validateUserCreateDto(final UserCreateDto createDto) {
        if (userRepository.findByEmail(createDto.getEmail()).isPresent()) {
            throw new DuplicateEntityForConditionException("user", "email");
        }
        if (userRepository.findByUsername(createDto.getUsername()).isPresent()) {
            throw new DuplicateEntityForConditionException("user", "username");
        }
    }

    private void validateUserUpdateDto(final UserType type) {
        // admin can MANAGE ONLY users
        if (securityService.hasRole(UserRole.ADMIN) && type != UserType.USER) {
            throw new AccessDeniedException("Operation not allowed");
        }
        if (securityService.hasRole(UserRole.SUPER_ADMIN) && type != UserType.ADMIN) {
            throw new AccessDeniedException("Operation not allowed");
        }
    }

    private void validateUserDelete(final UserType type) {
        // admin can MANAGE ONLY users
        if (securityService.hasRole(UserRole.ADMIN) && type != UserType.USER) {
            throw new AccessDeniedException("Operation not allowed");
        }
    }
    // endregion

}
