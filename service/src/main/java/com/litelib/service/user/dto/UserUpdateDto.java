package com.litelib.service.user.dto;

import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class UserUpdateDto {

    @Size(max = 50, message = "First name length is exceeded max size (50)")
    private String firstName;

    @Size(max = 50, message = "Last name length is exceeded max size (50)")
    private String lastName;

}
