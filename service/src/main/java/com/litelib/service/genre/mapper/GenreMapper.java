package com.litelib.service.genre.mapper;

import com.litelib.persistence.genre.Genre;
import com.litelib.service.genre.dto.GenreDto;
import org.mapstruct.Mapper;

import java.util.Set;

@Mapper(componentModel = "spring")
public interface GenreMapper {

    Set<GenreDto> mapEntitiesToDtoList(Set<Genre> genres);

    GenreDto mapEntityToDto(final Genre genre);


}
