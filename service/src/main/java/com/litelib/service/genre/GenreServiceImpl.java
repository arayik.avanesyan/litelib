package com.litelib.service.genre;

import com.litelib.persistence.genre.Genre;
import com.litelib.persistence.genre.GenreRepository;
import com.litelib.service.genre.dto.GenreDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Log4j2
@RequiredArgsConstructor
@Service
public class GenreServiceImpl implements GenreService {

    private final GenreRepository genreRepository;

    @Override
    public List<GenreDto> getAll() {
        return genreRepository.findAll()
                .stream().map(it -> new GenreDto(it.getId(), it.getName()))
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public Genre createAndGet(final String genre) {
        log.debug("Creating a genre");
        Assert.hasText(genre, "Genre is blank");

        final Optional<Genre> genreOpt = genreRepository.findByName(genre);
        if (genreOpt.isPresent()) {
            log.debug("Genre already exists");
            return genreOpt.get();
        }

        final Genre newGenre = new Genre();
        newGenre.setName(genre);
        return genreRepository.save(newGenre);
    }

}
