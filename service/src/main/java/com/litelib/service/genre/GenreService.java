package com.litelib.service.genre;

import com.litelib.persistence.genre.Genre;
import com.litelib.service.genre.dto.GenreDto;

import java.util.List;

public interface GenreService {

    List<GenreDto> getAll();

    Genre createAndGet(final String genre);

}
