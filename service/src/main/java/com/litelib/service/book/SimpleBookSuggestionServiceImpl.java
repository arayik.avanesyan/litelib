package com.litelib.service.book;

import com.litelib.persistence.book.Book;
import com.litelib.persistence.book.BookRepository;
import com.litelib.persistence.genre.Genre;
import com.litelib.persistence.userpreference.UserPreference;
import com.litelib.service.book.dto.BookDto;
import com.litelib.service.userpreference.UserPreferenceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Log4j2
@RequiredArgsConstructor
@Service
public class SimpleBookSuggestionServiceImpl implements BookSuggestionService {

    private final BookRepository bookRepository;

    private final UserPreferenceService userPreferenceService;

    private final BookMapper bookMapper;

    @Override
    public List<BookDto> suggestBooks(Integer page, Integer size, String search) {
        final UserPreference userPreference = userPreferenceService.getPreference();
        final Set<Genre> preferredGenres = userPreference.getGenres();
        final List<Book> books = bookRepository.findAllByGenresInAndSearch(preferredGenres, search, PageRequest.of(page, size));
        return bookMapper.mapEntityListToDtoList(books);
    }

    @Override
    public Long getSuggestBooksCount(String search) {
        final UserPreference userPreference = userPreferenceService.getPreference();
        final Set<Genre> preferredGenres = userPreference.getGenres();
        return bookRepository.countAllByGenresInAndSearch(preferredGenres, search);
    }

}
