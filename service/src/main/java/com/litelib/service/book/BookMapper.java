package com.litelib.service.book;

import com.litelib.persistence.book.Book;
import com.litelib.service.author.mapper.AuthorMapper;
import com.litelib.service.book.dto.BookCreateDto;
import com.litelib.service.book.dto.BookDto;
import com.litelib.service.genre.mapper.GenreMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {AuthorMapper.class, GenreMapper.class})
public interface BookMapper {

    @Mapping(target = "inStock", source = "book.inventory.count")
    BookDto mapEntityToDto(final Book book);

    List<BookDto> mapEntityListToDtoList(final List<Book> books);

    @Mapping(target = "authors", ignore = true)
    Book mapCreateDtoToEntity(final BookCreateDto book);

    List<Book> mapCreateDtoListToEntityList(final List<BookCreateDto> books);

}
