package com.litelib.service.book.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Data;

@Data
public class BookPurchaseDto {

    @Positive
    @NotNull(message = "Book quantity is null")
    private Long quantity;

}
