package com.litelib.service.book.dto;

import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class BookDetailsDto {
    private Long id;
    private String title;
    private String author;
    private String genre;
    private String description;
    private String isbn;
    private String image;
    private ZonedDateTime published;
    private String publisher;
    private Integer total;
}
