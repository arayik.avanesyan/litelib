package com.litelib.service.book;

import com.litelib.service.book.dto.BookDto;

import java.util.List;

public interface BookSuggestionService {

    List<BookDto> suggestBooks(Integer page, Integer size, String search);

    Long getSuggestBooksCount(final String search);
}
