package com.litelib.service.book.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class BookCreateDto {

    @NotBlank(message = "title is required")
    private String title;

    @NotBlank(message = "author is required")
    private String author;

    @NotBlank(message = "genre is required")
    private String genre;

    private String description;

    @NotBlank(message = "isbn is required")
    private String isbn;

    private String image;

    private ZonedDateTime published;

    private String publisher;

}
