package com.litelib.service.book.dto;

import com.litelib.service.booktoauthor.dto.AuthorDto;
import com.litelib.service.genre.dto.GenreDto;
import lombok.Data;

import java.time.ZonedDateTime;
import java.util.Set;

@Data
public class BookDto {
    private Long id;
    private String title;
    private Set<AuthorDto> authors;
    private Set<GenreDto> genres;
    private String description;
    private String isbn;
    private String image;
    private ZonedDateTime published;
    private String publisher;
    private Integer inStock;
}
