package com.litelib.service.book;

import com.litelib.service.book.dto.BookCreateDto;
import com.litelib.service.book.dto.BookDto;
import com.litelib.service.book.dto.BookPurchaseDto;

import java.util.List;

public interface BookService {

    BookDto get(final Long id);

    List<BookDto> getAll(Integer page, Integer size, String search);

    Long count(String search);

    Long create(final BookCreateDto createDto);

    void batchCreate(final List<BookCreateDto> bookDtoList);

    void purchases(Long id, final BookPurchaseDto dto);
}
