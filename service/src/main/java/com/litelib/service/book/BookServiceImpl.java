package com.litelib.service.book;

import com.litelib.persistence.author.Author;
import com.litelib.persistence.book.Book;
import com.litelib.persistence.book.BookRepository;
import com.litelib.persistence.bookinventory.BookInventory;
import com.litelib.persistence.common.AbstractAuditableDomainEntity;
import com.litelib.persistence.genre.Genre;
import com.litelib.service.author.AuthorService;
import com.litelib.service.book.dto.BookCreateDto;
import com.litelib.service.book.dto.BookDto;
import com.litelib.service.book.dto.BookPurchaseDto;
import com.litelib.service.bookinventory.BookInventoryService;
import com.litelib.service.bookpurchasehistory.BookPurchaseHistoryService;
import com.litelib.service.booktoauthor.BookToAuthorService;
import com.litelib.service.genre.GenreService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Log4j2
@RequiredArgsConstructor
@Service
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    private final AuthorService authorService;

    private final BookToAuthorService bookToAuthorService;

    private final BookMapper bookMapper;

    private final BookInventoryService bookInventoryService;

    private final BookPurchaseHistoryService bookPurchaseHistoryService;

    private final GenreService genreService;

    @Override
    public BookDto get(final Long id) {
        log.info("Loading a book by id - {}", id);
        Assert.notNull(id, "Book id is null");
        final Book book = getBook(id);
        Assert.notNull(book, "Book was not found");
        return bookMapper.mapEntityToDto(book);
    }

    @Override
    public List<BookDto> getAll(Integer page, Integer size, String search) {
        log.info("Loading books");
        final List<Book> books = bookRepository.findAll(search, PageRequest.of(page, size));
        return bookMapper.mapEntityListToDtoList(books);
    }

    @Override
    public Long count(String search) {
        log.info("Counting books");
        return bookRepository.countAll(search);
    }

    @Transactional
    @Override
    public Long create(final BookCreateDto createDto) {
        log.info("Saving a book");
        Assert.notNull(createDto, "BookCreateDto is null");

        Optional<Book> bookByIsbn = bookRepository.findByIsbn(createDto.getIsbn());
        if (bookByIsbn.isPresent()) {
            log.info("Book with isbn {} already exists. Updating inventory", createDto.getIsbn());
            final Book book = bookByIsbn.get();
            bookInventoryService.updateInventory(book.getId(), 1L);
            return book.getId();
        }

        final Book book = bookMapper.mapCreateDtoToEntity(createDto);
        book.setInventory(new BookInventory(book, 1L));

        Book saved = bookRepository.save(book);
        final Author author = authorService.createAndGet(createDto.getAuthor());
        bookToAuthorService.create(book, author);

        final Genre genre = genreService.createAndGet(createDto.getGenre());
        saved.getGenres().add(genre);
        genre.getBooks().add(saved);

        return saved.getId();
    }

    @Transactional
    @Override
    public void batchCreate(final List<BookCreateDto> bookDtoList) {
        log.info("Saving {} books", bookDtoList.size());
        bookDtoList.forEach(this::validateDto);

        final Set<String> isbnSet = bookDtoList.stream().map(BookCreateDto::getIsbn).collect(Collectors.toSet());

        final List<Book> books = bookRepository.findAllByIsbnIn(isbnSet);
        final Map<String, Book> isbnBookMap = books
                .stream()
                .collect(Collectors.toMap(Book::getIsbn, Function.identity()));
        bookDtoList.forEach(this::create);

        // update inventory for old books
        Map<Long, Long> collect = isbnBookMap.values().stream()
                .collect(Collectors.toMap(AbstractAuditableDomainEntity::getId, th -> 1L));
        bookInventoryService.updateInventory(collect);
    }

    @Transactional
    @Override
    public void purchases(Long id, final BookPurchaseDto dto) {
        log.info("Purchasing books");
        Assert.notNull(dto, "BookPurchaseDto is null");
        bookInventoryService.purchase(id, dto.getQuantity());
        final Book book = getBook(id);
        bookPurchaseHistoryService.create(book, dto.getQuantity());
    }

    private Book getBook(final Long bookId) {
        return bookRepository.findById(bookId)
                .orElseThrow(() -> new EntityNotFoundException("Book was not found"));
    }

    private void validateDto(final BookCreateDto createDto) {
        Assert.notNull(createDto, "BookCreateDto is null");
        Assert.hasText(createDto.getAuthor(), "Author is required");
        Assert.hasText(createDto.getTitle(), "Title is required");
    }

}
