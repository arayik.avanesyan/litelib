package com.litelib.service.exceptions;

public class ServiceRuntimeException extends RuntimeException {

    public ServiceRuntimeException(final String message) {
        super(message);
    }

}
