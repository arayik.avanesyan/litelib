package com.litelib.service.exceptions;

public class DuplicateEntityForConditionException extends RuntimeException {

    public DuplicateEntityForConditionException(final String entity, final String condition) {
        super(String.format("%s exists with %s", entity, condition));
    }

}
