package com.litelib.service.exceptions;

import lombok.Getter;

@Getter
public class CsvParsingRuntimeException extends RuntimeException {

    private final Exception cause;

    public CsvParsingRuntimeException(final Exception cause, final String message) {
        super(message);
        this.cause = cause;
    }

}
