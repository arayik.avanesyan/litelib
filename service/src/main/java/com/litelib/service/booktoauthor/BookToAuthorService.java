package com.litelib.service.booktoauthor;

import com.litelib.persistence.author.Author;
import com.litelib.persistence.book.Book;

public interface BookToAuthorService {

    void create(final Book book, final Author author);

}
