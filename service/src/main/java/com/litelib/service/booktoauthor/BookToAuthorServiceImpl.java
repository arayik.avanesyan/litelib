package com.litelib.service.booktoauthor;

import com.litelib.persistence.author.Author;
import com.litelib.persistence.book.Book;
import com.litelib.persistence.bookauthor.BookToAuthor;
import com.litelib.persistence.bookauthor.BookToAuthorRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Log4j2
@RequiredArgsConstructor
@Service
public class BookToAuthorServiceImpl implements BookToAuthorService {

    private final BookToAuthorRepository bookToAuthorRepository;

    @Transactional
    @Override
    public void create(Book book, Author author) {
        Assert.notNull(book, "Book is null");
        Assert.notNull(author, "Author is null");
        if (!bookToAuthorRepository.existsByBookAndAuthor(book, author)) {
            bookToAuthorRepository.save(new BookToAuthor(book, author));
        }
    }
}
