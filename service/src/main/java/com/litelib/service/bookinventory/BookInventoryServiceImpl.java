package com.litelib.service.bookinventory;

import com.litelib.persistence.bookinventory.BookInventory;
import com.litelib.service.exceptions.OutOfStockException;
import jakarta.persistence.OptimisticLockException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Map;

@Log4j2
@RequiredArgsConstructor
@Service
public class BookInventoryServiceImpl implements BookInventoryService {

    private final BookInventoryRepository bookInventoryRepository;

    @Override
    public boolean checkAvailability(final Long bookId, final Long quantity) {
        log.info("Checking availability of book: {}", bookId);
        final BookInventory bookInventory = getByBookId(bookId);
        return bookInventory.getCount() - quantity >= 0;
    }

    @Retryable(retryFor = {OptimisticLockingFailureException.class, OptimisticLockException.class})
    @Transactional
    @Override
    public void purchase(final Long bookId, final Long quantity) {
        log.info("Purchasing a book: {}", bookId);
        if (!checkAvailability(bookId, quantity)) {
            log.info("No available book: {}", bookId);
            throw new OutOfStockException("Books is out of stock");
        }
        final BookInventory bookInventory = getByBookId(bookId);
        final Long count = bookInventory.getCount();
        bookInventory.setCount(count - quantity);
        bookInventoryRepository.save(bookInventory);
        log.info("The book {} was purchased", bookId);
    }

    @Retryable(retryFor = {OptimisticLockingFailureException.class, OptimisticLockException.class})
    @Transactional
    @Override
    public void updateInventory(final Long bookId, final Long quantity) {
        log.info("Adding new books: {}", bookId);
        final BookInventory bookInventory = getByBookId(bookId);
        final Long count = bookInventory.getCount();
        bookInventory.setCount(count + quantity);
        bookInventoryRepository.save(bookInventory);
    }

    @Retryable(retryFor = {OptimisticLockingFailureException.class, OptimisticLockException.class})
    @Transactional
    @Override
    public void updateInventory(final Map<Long, Long> idQuantityMap) {

        final List<BookInventory> bookInventory = bookInventoryRepository.findByBookIdIn(idQuantityMap.keySet());

        for (BookInventory inventory : bookInventory) {
            Long quantity = idQuantityMap.get(inventory.getBook().getId());
            inventory.setCount(inventory.getCount() + quantity);
        }

        bookInventoryRepository.saveAll(bookInventory);
    }

    private BookInventory getByBookId(final Long bookId) {
        Assert.notNull(bookId, "bookId is null");
        final BookInventory inventory = bookInventoryRepository.findByBookId(bookId);
        Assert.notNull(inventory, "Inventory was not found");
        return inventory;
    }

}
