package com.litelib.service.bookinventory;

import com.litelib.persistence.bookinventory.BookInventory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Set;

public interface BookInventoryRepository extends JpaRepository<BookInventory, Long> {

    BookInventory findByBookId(final Long bookId);

    List<BookInventory> findByBookIdIn(final Set<Long> bookId);

}
