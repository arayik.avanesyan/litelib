package com.litelib.service.bookinventory;

import java.util.Map;

public interface BookInventoryService {

    boolean checkAvailability(final Long bookId, final Long quantity);

    void purchase(final Long bookId, final Long quantity);

    void updateInventory(final Long bookId, final Long quantity);

    void updateInventory(final Map<Long, Long> idQuantityMap);
}
