package com.litelib.service.userpreference.dto;

import lombok.Data;

import java.util.Set;

@Data
public class UserPreferenceDto {

    private Set<Long> genreIds;

}
