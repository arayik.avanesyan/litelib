package com.litelib.service.userpreference;

import com.litelib.persistence.genre.Genre;
import com.litelib.persistence.genre.GenreRepository;
import com.litelib.persistence.user.User;
import com.litelib.persistence.userpreference.UserPreference;
import com.litelib.persistence.userpreference.UserPreferenceRepository;
import com.litelib.service.user.UserService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Log4j2
@RequiredArgsConstructor
@Service
public class UserPreferenceServiceImpl implements UserPreferenceService {

    private final UserPreferenceRepository userPreferenceRepository;

    private final GenreRepository genreRepository;

    private final UserService userService;

    @Transactional
    @Override
    public UserPreference getPreference() {
        final User user = userService.getUser();
        return userPreferenceRepository.findByUser(user)
                .orElseGet(() -> creatDefaultUserPreference(user));
    }

    @Transactional
    @Override
    public UserPreference updateOrCreateUserPreference(Set<Long> genreIds) {
        log.debug("Saving user preferences: {}", genreIds);
        final User user = userService.getUser();
        final Set<Genre> genres = genreRepository.findAllByIdIn(genreIds);

        final UserPreference preference = userPreferenceRepository.findByUser(user)
                .orElseThrow(() -> new EntityNotFoundException("UserPreference was not found"));

        preference.setUser(user);
        preference.setGenres(genres);

        return userPreferenceRepository.save(preference);
    }

    private UserPreference creatDefaultUserPreference(User user) {
        final UserPreference preference = new UserPreference();
        preference.setUser(user);
        preference.setGenres(new HashSet<>());
        return userPreferenceRepository.save(preference);
    }

}
