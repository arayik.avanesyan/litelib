package com.litelib.service.userpreference;

import com.litelib.persistence.userpreference.UserPreference;

import java.util.Set;

public interface UserPreferenceService {

    UserPreference getPreference();

    UserPreference updateOrCreateUserPreference(Set<Long> genreIds);

}
