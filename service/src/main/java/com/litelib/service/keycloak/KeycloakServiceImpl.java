package com.litelib.service.keycloak;

import com.litelib.service.exceptions.DuplicateEntityForConditionException;
import com.litelib.service.exceptions.ServiceRuntimeException;
import com.litelib.service.keycloak.dto.KeycloakUserCreateDto;
import com.litelib.service.keycloak.dto.KeycloakUserUpdateDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.Response;
import java.util.Collections;

@Log4j2
@RequiredArgsConstructor
@Service
public class KeycloakServiceImpl implements KeycloakService {

    private final KeycloakClient keycloakClient;

    @Override
    public UserRepresentation getByUsername(final String username) {
        return keycloakClient.getUserByUsername(username);
    }

    @Override
    public String createUser(final KeycloakUserCreateDto keycloakUserCreateDto) {
        validateCreateDto(keycloakUserCreateDto);
        if (keycloakClient.userExistsByEmail(keycloakUserCreateDto.getEmail())) {
            throw new DuplicateEntityForConditionException("sso user", "username");
        }
        UserRepresentation userRepresentation = mapToUserRepresentation(keycloakUserCreateDto);
        final UsersResource usersResource = keycloakClient.getUsersResource();
        final Response response = usersResource.create(userRepresentation);
        if (response.getStatus() == HttpStatus.UNAUTHORIZED.value()) {
            throw new NotAuthorizedException("Keycloak authorization exception");
        }
        if (response.getStatus() == HttpStatus.CONFLICT.value()) {
            throw new DuplicateEntityForConditionException("user", "username");
        }
        if (response.getStatus() != HttpStatus.CREATED.value()) {
            // todo refactor to handle properly
            throw new ServiceRuntimeException("Something went wrong");
        }
        userRepresentation = getByUsername(keycloakUserCreateDto.getUsername());
        final UserResource userResource = usersResource.get(userRepresentation.getId());
        userResource.update(userRepresentation);
        keycloakClient.addUserToGroup(userRepresentation.getId(), keycloakUserCreateDto.getGroup());
        return userRepresentation.getId();
    }

    @Override
    public void updateUser(final KeycloakUserUpdateDto updateDto) {
        validateUpdateDto(updateDto);
        final var userRepresentationOpt = keycloakClient.findUserByEmail(updateDto.getEmail());
        userRepresentationOpt
                .ifPresent(userRepresentation -> {
                    if (!userRepresentation.getId().equals(updateDto.getSsoId())) {
                        throw new IllegalArgumentException("User exists with such email");
                    }
                    final var userResource = keycloakClient.getUserResource(updateDto.getSsoId());
                    mapToUserRepresentation(userRepresentation, updateDto);
                    userResource.update(userRepresentation);
                });
    }

    @Override
    public void disableUser(final String email, final String ssoId) {

        final var userResource = keycloakClient.getUserResource(ssoId);
        keycloakClient.findUserByEmail(email)
                .ifPresent(userRepresentation -> {
                    userRepresentation.setEnabled(false);
                    userResource.update(userRepresentation);
                });
    }

    private UserRepresentation mapToUserRepresentation(final KeycloakUserCreateDto keycloakUserCreateDto) {
        final UserRepresentation userRepresentation = new UserRepresentation();
        userRepresentation.setUsername(keycloakUserCreateDto.getUsername());
        userRepresentation.setFirstName(keycloakUserCreateDto.getFirstName());
        userRepresentation.setLastName(keycloakUserCreateDto.getLastName());
        userRepresentation.setEnabled(keycloakUserCreateDto.isEnabled());
        userRepresentation.setEmailVerified(true);
        userRepresentation.setEmail(keycloakUserCreateDto.getEmail());
        setPassword(userRepresentation, keycloakUserCreateDto.getPassword(), keycloakUserCreateDto.isTempPassword());
        return userRepresentation;
    }

    private void mapToUserRepresentation(final UserRepresentation userRepresentation,
                                         final KeycloakUserUpdateDto updateDto) {
        userRepresentation.setFirstName(updateDto.getFirstName());
        userRepresentation.setLastName(updateDto.getLastName());
        userRepresentation.setEmailVerified(true);
        userRepresentation.setEmail(updateDto.getEmail());
    }

    private void setPassword(final UserRepresentation userRepresentation, final String password, boolean tempPassword) {
        final CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
        credentialRepresentation.setValue(password);
        credentialRepresentation.setTemporary(tempPassword);
        userRepresentation.setCredentials(Collections.singletonList(credentialRepresentation));
    }

    private void validateCreateDto(final KeycloakUserCreateDto userCreateDto) {
        Assert.notNull(userCreateDto, "KeycloakUserCreateDto is null");
        Assert.isTrue(StringUtils.isNotBlank(userCreateDto.getUsername()), "Username is null or is blank");
        Assert.isTrue(StringUtils.isNotBlank(userCreateDto.getEmail()), "Email is null or is blank");
        Assert.isTrue(StringUtils.isNotBlank(userCreateDto.getPassword()), "Password is null or blank");
    }

    private void validateUpdateDto(final KeycloakUserUpdateDto userUpdateDto) {
        Assert.notNull(userUpdateDto, "KeycloakUserUpdateDto is null");
        Assert.isTrue(StringUtils.isNotBlank(userUpdateDto.getSsoId()), "SsoId is null or is blank");
        Assert.isTrue(StringUtils.isNotBlank(userUpdateDto.getEmail()), "Email is null or is blank");
    }


}
