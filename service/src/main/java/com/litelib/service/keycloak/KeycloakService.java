package com.litelib.service.keycloak;

import com.litelib.service.keycloak.dto.KeycloakUserCreateDto;
import com.litelib.service.keycloak.dto.KeycloakUserUpdateDto;
import org.keycloak.representations.idm.UserRepresentation;

public interface KeycloakService {

    UserRepresentation getByUsername(final String username);

    String createUser(final KeycloakUserCreateDto keycloakUserCreateDto);

    void updateUser(final KeycloakUserUpdateDto keycloakUserUpdateDto);

    void disableUser(final String email, final String ssoId);

}
