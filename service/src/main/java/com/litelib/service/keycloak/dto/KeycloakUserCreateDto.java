package com.litelib.service.keycloak.dto;

import com.litelib.persistence.user.UserGroup;
import lombok.Data;

@Data
public class KeycloakUserCreateDto {

    private String username;
    private String firstName;
    private String lastName;
    private boolean enabled;
    private String email;
    private String password;
    private boolean tempPassword;
    private UserGroup group;

}
