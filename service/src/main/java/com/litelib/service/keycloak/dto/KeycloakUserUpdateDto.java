package com.litelib.service.keycloak.dto;

import lombok.Data;

@Data
public class KeycloakUserUpdateDto {
    private String ssoId;
    private String firstName;
    private String lastName;
    private String email;
}
