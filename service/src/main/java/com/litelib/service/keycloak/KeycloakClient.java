package com.litelib.service.keycloak;

import com.litelib.persistence.user.UserGroup;
import jakarta.persistence.EntityNotFoundException;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class KeycloakClient {

    private final RealmResource realmResource;

    public KeycloakClient(final KeycloakProperties keycloakProperties) {
        this.realmResource = KeycloakBuilder.builder()
                .serverUrl(keycloakProperties.getUrl())
                .realm(keycloakProperties.getRealm())
                .grantType(OAuth2Constants.CLIENT_CREDENTIALS)
                .clientId(keycloakProperties.getClientId())
                .clientSecret(keycloakProperties.getClientSecret())
                .build()
                .realm(keycloakProperties.getRealm());
    }

    public UserRepresentation getUserByUsername(final String username) {
        final List<UserRepresentation> users = realmResource.users().search(username);
        if (users.isEmpty()) {
            throw new EntityNotFoundException("User not found for username " + username);
        }
        return users.get(0);
    }

    public Optional<UserRepresentation> findUserByEmail(final String email) {
        return realmResource.users()
                .search(null, null, null, email, 0, 1)
                .stream()
                .findFirst();
    }

    public boolean userExistsByEmail(final String email) {
        return findUserByEmail(email).isPresent();
    }

    public UsersResource getUsersResource() {
        return realmResource.users();
    }

    public UserResource getUserResource(String id) {
        return getUsersResource().get(id);
    }
    // endregion

    public void addUserToGroup(final String userSsoId, final UserGroup group) {
        final var userResource = getUsersResource().get(userSsoId);
        final var groupRepresentation = getGroup(group);
        userResource.joinGroup(groupRepresentation.getId());
    }

    public GroupRepresentation getGroup(final UserGroup group) {
        return this.realmResource.groups().groups().stream()
                .filter(it -> group.getGroupName().equals(it.getName()))
                .findFirst()
                .orElseThrow();
    }

}
