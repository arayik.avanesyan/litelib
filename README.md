### Resources

- Books data will be read from [this API](https://fakerapi.it/api/v1/books?_quantity=100&_locale=en_US)
- Users data will be read from [this csv](https://drive.google.com/file/d/1HEL41GCv1LCX8b2rqkBL3rIDc3TJva4P/view)

### Prerequisites

- Make sure you have Docker Desktop installed

### Components

The project consist of 4 components.

1. The database
2. The authentication/authorization server
3. The back-end
4. The front-end

### How to run each component

1. The `database` can be run from [compose.yml](compose.yaml) - the `postgres` service
   - The init script will run to create DB for keycloak service
2. The `auth server` can be run from [compose.yml](compose.yaml) - the `keycloak` service
    - It will run keycloak service
        - will import litelib realm (with preconfigured client within it)
    - Open the browser in http://localhost:8080 and login
        - ```username: admin``` and ```password: admin```
    - In the left top cornet select the `Litelib` realm
3. The `backend`
    - run the api module with `dev` active profile (it is set from Edit Configurations of Intellij)
      so it can pick up the ready config file [application-dev.yml](api%2Fsrc%2Fmain%2Fresources%2Fapplication-dev.yml).
        - At startup, it will create initial user with SUPER_ADMIN role.
        - `username: testuser` and `password:123456`
    - **it is dockerized**
       - `mvn clean package`
       - `docker build . -t litelib:latest`
4. The `frontend`
    - clone the front end app from [here](https://gitlab.com/arayik.avanesyan/litelib-ui.git)
    - make sure you have node installed
    - after which install node dependencies by running `npm i`
    - then from terminal run `ng serve`
   - ui will be accessible on http://localhost:4200
    - test user `username: testuser` and `password: 123456`

## Notes:

#### There was issue with related to communication between service during authentication when running all services from docker.

During authentication process when client navigate to keycloak page and authenticates -
it receives a token where the `iss` claim is set to `localhost` later backend receives that  
token and try to validate by requesting keycloak (to which it talks via docker `service name`, not by `localhost`).
So keycloak validate the token and gives 401. `It is required to run both services locally`

