package com.litelib.persistence.book;

import com.litelib.persistence.genre.Genre;
import com.litelib.persistence.bookauthor.BookToAuthor;
import com.litelib.persistence.bookinventory.BookInventory;
import com.litelib.persistence.common.AbstractAuditableDomainEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "books")
public class Book extends AbstractAuditableDomainEntity {

    @Column(name = "title", nullable = false)
    private String title;

    @OneToMany(mappedBy = "book")
    private Set<BookToAuthor> authors;

    @ManyToMany(mappedBy = "books")
    private Set<Genre> genres = new HashSet<>();

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    @Column(name = "isbn", nullable = false, unique = true)
    private String isbn;

    @Column(name = "image")
    private String image;

    @Column(name = "published", columnDefinition = "timestamp with time zone")
    private ZonedDateTime published;

    @Column(name = "publisher")
    private String publisher;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "inventory_id", referencedColumnName = "id", nullable = false)
    private BookInventory inventory;

}
