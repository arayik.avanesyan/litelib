package com.litelib.persistence.book;

import com.litelib.persistence.genre.Genre;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface BookRepository extends JpaRepository<Book, Long> {

    Optional<Book> findByIsbn(final String isbn);

    List<Book> findAllByIsbnIn(final Set<String> isbnSet);

    @Query("select distinct b FROM Book b join b.genres g where (g.name in ?1) or (b.title like %?1%) or (b.publisher like %?1%)")
    List<Book> findAll(final String search, final PageRequest pageRequest);

    @Query("select distinct count(b) FROM Book b join b.genres g where (g.name in ?1) or (b.title like %?1%) or (b.publisher like %?1%)")
    Long countAll(final String search);

    // with preference
    @Query("select distinct b FROM Book b join b.genres g where (g in ?1) AND ((g.name in ?2) or (b.title like %?2%) or (b.publisher like %?2%))")
    List<Book> findAllByGenresInAndSearch(final Set<Genre> genres, final String search, PageRequest pageRequest);

    // with preference
    @Query("select distinct count(b) FROM Book b join b.genres g where (g in ?1) AND ((g.name in ?2) or (b.title like %?2%) or (b.publisher like %?2%))")
    Long countAllByGenresInAndSearch(final Set<Genre> preferredGenres, final String search);

}
