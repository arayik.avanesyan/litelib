package com.litelib.persistence.genre;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.Set;

public interface GenreRepository extends JpaRepository<Genre, Long> {

    Optional<Genre> findByName(final String genre);

    Set<Genre> findAllByIdIn(final Set<Long> idSet);

}
