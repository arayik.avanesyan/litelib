package com.litelib.persistence.genre;

import com.litelib.persistence.book.Book;
import com.litelib.persistence.common.AbstractAuditableDomainEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "genres")
public class Genre extends AbstractAuditableDomainEntity {

    @Column(name = "name")
    private String name;

    @ManyToMany
    Set<Book> books = new HashSet<>();


}
