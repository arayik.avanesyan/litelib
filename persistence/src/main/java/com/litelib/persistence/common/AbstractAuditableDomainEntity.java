package com.litelib.persistence.common;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serializable;
import java.time.Clock;
import java.time.ZonedDateTime;

@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
@MappedSuperclass
public class AbstractAuditableDomainEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @CreatedBy
    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_at", columnDefinition = "timestamp with time zone")
    private ZonedDateTime createdAt;

    @LastModifiedBy
    @Column(name = "modified_by")
    private String modifiedBy;

    @Column(name = "modified_at", columnDefinition = "timestamp with time zone")
    private ZonedDateTime modifiedAt;

    @Column(name = "removed_at", columnDefinition = "timestamp with time zone")
    private ZonedDateTime removeAt;

    @Column(name = "active")
    private boolean active;

    public AbstractAuditableDomainEntity() {
        setActive(true);
    }

    @PrePersist
    public void beforePersist() {
        setCreatedAt(ZonedDateTime.now(Clock.systemUTC()));
        setModifiedAt(getCreatedAt());
    }

    @PreUpdate
    public void beforeUpdate() {
        setModifiedAt(ZonedDateTime.now(Clock.systemUTC()));
    }

    public void markRemoved() {
        setActive(false);
        setRemoveAt(ZonedDateTime.now(Clock.systemUTC()));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof AbstractAuditableDomainEntity that)) return false;

        return new EqualsBuilder()
                .append(id, that.id)
                .append(createdAt, that.createdAt)
                .append(modifiedAt, that.modifiedAt)
                .append(removeAt, that.removeAt)
                .append(active, that.active)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(createdAt)
                .append(modifiedAt)
                .append(removeAt)
                .append(active)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("createdAt", createdAt)
                .append("modifiedAt", modifiedAt)
                .append("removeAt", removeAt)
                .append("active", active)
                .toString();
    }
}
