package com.litelib.persistence.audit;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;

import java.util.Optional;

public class AuditorAwareImpl implements AuditorAware<String> {

    private static final String SYSTEM = "SYSTEM";

    private static final String PREFERRED_USERNAME = "preferred_username";

    @Override
    public Optional<String> getCurrentAuditor() {
        final Optional<String> userOpt = Optional.ofNullable(SecurityContextHolder.getContext())
                .map(SecurityContext::getAuthentication)
                .map(Authentication::getPrincipal)
                .map(Jwt.class::cast)
                .map(jwt -> jwt.getClaimAsString(PREFERRED_USERNAME));;
        if (userOpt.isEmpty()) {
            return Optional.of(SYSTEM);
        }
        return userOpt;
    }

}
