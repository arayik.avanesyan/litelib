package com.litelib.persistence.user;

public enum UserRole {
    SUPER_ADMIN,
    ADMIN,
    USER,
    ;

    public static UserRole getByValue(final String value) {
        if (SUPER_ADMIN.name().equals(value)) {
            return SUPER_ADMIN;
        }
        if (ADMIN.name().equals(value)) {
            return ADMIN;
        }
        if (USER.name().equals(value)) {
            return USER;
        }
        return null;
    }
}
