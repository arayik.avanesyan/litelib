package com.litelib.persistence.user;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UserGroup {
    ADMINS("Admins"),
    SUPER_ADMINS("Super-Admins"),
    USERS("Users"),
    ;

    private final String groupName;

}
