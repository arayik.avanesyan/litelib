package com.litelib.persistence.user;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UserType {
    SUPER_ADMIN(UserGroup.SUPER_ADMINS),
    ADMIN(UserGroup.ADMINS),
    USER(UserGroup.USERS),
    ;

    private final UserGroup group;

}
