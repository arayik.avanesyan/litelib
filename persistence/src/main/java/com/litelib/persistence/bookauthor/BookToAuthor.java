package com.litelib.persistence.bookauthor;

import com.litelib.persistence.author.Author;
import com.litelib.persistence.book.Book;
import com.litelib.persistence.common.AbstractAuditableDomainEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "book_to_author")
public class BookToAuthor extends AbstractAuditableDomainEntity {

    @ManyToOne
    @JoinColumn(name = "book_id")
    private Book book;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private Author author;


}
