package com.litelib.persistence.bookauthor;

import com.litelib.persistence.author.Author;
import com.litelib.persistence.book.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookToAuthorRepository extends JpaRepository<BookToAuthor, Long> {

    boolean existsByBookAndAuthor(final Book book, final Author author);

}
