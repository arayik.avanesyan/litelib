package com.litelib.persistence.userpreference;

import com.litelib.persistence.common.AbstractAuditableDomainEntity;
import com.litelib.persistence.genre.Genre;
import com.litelib.persistence.user.User;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "user_preferences")
public class UserPreference extends AbstractAuditableDomainEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private User user;

    @ManyToMany
    private Set<Genre> genres = new HashSet<>();
}
