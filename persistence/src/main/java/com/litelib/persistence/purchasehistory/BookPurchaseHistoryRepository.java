package com.litelib.persistence.purchasehistory;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BookPurchaseHistoryRepository extends JpaRepository<BookPurchaseHistory, Long> {
}
