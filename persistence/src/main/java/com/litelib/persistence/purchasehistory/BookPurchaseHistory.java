package com.litelib.persistence.purchasehistory;

import com.litelib.persistence.book.Book;
import com.litelib.persistence.common.AbstractAuditableDomainEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Getter
@Setter
@Entity
@Table(name = "book_purchase_history")
public class BookPurchaseHistory extends AbstractAuditableDomainEntity {

    @ManyToOne
    private Book book;

    @Column(name = "quantity", nullable = false)
    private Long quantity;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof BookPurchaseHistory that)) return false;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(book.getId(), that.book.getId())
                .append(quantity, that.quantity)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(book.getId())
                .append(quantity)
                .toHashCode();
    }
}
