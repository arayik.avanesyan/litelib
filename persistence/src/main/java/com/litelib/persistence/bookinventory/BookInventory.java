package com.litelib.persistence.bookinventory;

import com.litelib.persistence.book.Book;
import com.litelib.persistence.common.AbstractVersionedAuditableDomainEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "book_inventory")
public class BookInventory extends AbstractVersionedAuditableDomainEntity {

    @OneToOne(mappedBy = "inventory", fetch = FetchType.LAZY)
    private Book book;

    @Column(name = "count")
    private Long count;

}
