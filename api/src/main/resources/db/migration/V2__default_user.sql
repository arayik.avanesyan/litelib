insert into users (active, created_at, created_by, modified_at, modified_by, email, first_name,
                   last_name, sso_id, type, username)
values (true, now(), 'SYSTEM', now(), 'SYSTEM', 'testuser@gmail.com', 'Test', 'User', 'SSO_ID', 'SUPER_ADMIN', 'testuser');