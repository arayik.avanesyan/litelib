-- region author
create sequence public.authors_id_seq;
alter sequence public.authors_id_seq owner to myuser;
create table public.authors
(
    id          bigint primary key     not null default nextval('authors_id_seq'::regclass),
    active      boolean,
    created_at  timestamp with time zone,
    created_by  character varying(255),
    modified_at timestamp with time zone,
    modified_by character varying(255),
    removed_at  timestamp with time zone,
    bio         text,
    first_name  character varying(255) not null,
    last_name   character varying(255) not null
);
create unique index uk6va0jjhs08lootjg6xosj1b2d on authors using btree (first_name, last_name);
create index idx_author_bio on public.authors using gin (to_tsvector('english'::regconfig, bio));
alter sequence public.authors_id_seq owned by public.authors.id;
-- endregion

-- region book_inventory
create sequence public.book_inventory_id_seq;
alter sequence public.book_inventory_id_seq owner to myuser;
create table public.book_inventory
(
    id          bigint primary key not null default nextval('book_inventory_id_seq'::regclass),
    active      boolean,
    created_at  timestamp with time zone,
    created_by  character varying(255),
    modified_at timestamp with time zone,
    modified_by character varying(255),
    removed_at  timestamp with time zone,
    version     bigint,
    count       bigint
);
alter sequence public.book_inventory_id_seq owned by public.book_inventory.id;
-- endregion

-- region genre
create sequence public.genres_id_seq;
alter sequence public.genres_id_seq owner to myuser;
create table public.genres
(
    id          bigint primary key not null default nextval('genres_id_seq'::regclass),
    active      boolean,
    created_at  timestamp with time zone,
    created_by  character varying(255),
    modified_at timestamp with time zone,
    modified_by character varying(255),
    removed_at  timestamp with time zone,
    name        character varying(255)
);
alter sequence public.genres_id_seq owned by public.genres.id;
-- endregion

-- region books
create sequence public.books_id_seq;
alter sequence public.books_id_seq owner to myuser;
create table public.books
(
    id           bigint primary key     not null default nextval('books_id_seq'::regclass),
    active       boolean,
    created_at   timestamp with time zone,
    created_by   character varying(255),
    modified_at  timestamp with time zone,
    modified_by  character varying(255),
    removed_at   timestamp with time zone,
    description  text,
    image        character varying(255),
    isbn         character varying(255) not null,
    published    timestamp with time zone,
    publisher    character varying(255),
    title        character varying(255) not null,
    inventory_id bigint                 not null,
    foreign key (inventory_id) references public.book_inventory (id)
);
create unique index uk_kibbepcitr0a3cpk3rfr7nihn on books using btree (isbn);
create unique index uk_3mr54mj0kbqxdyrr2f8a02jxd on books using btree (inventory_id);
create index idx_book_description on public.books using gin (to_tsvector('english'::regconfig, description));
alter sequence public.books_id_seq owned by public.books.id;
-- endregion

-- region genres_books
create table public.genres_books
(
    genres_id bigint not null,
    books_id  bigint not null,
    primary key (genres_id, books_id),
    foreign key (books_id) references public.books (id)
        match simple on update no action on delete no action,
    foreign key (genres_id) references public.genres (id)
        match simple on update no action on delete no action
);
-- endregion

-- region book_purchase_history
create sequence public.book_purchase_history_id_seq;
alter sequence public.book_purchase_history_id_seq owner to myuser;
create table public.book_purchase_history
(
    id          bigint primary key not null default nextval('book_purchase_history_id_seq'::regclass),
    active      boolean,
    created_at  timestamp with time zone,
    created_by  character varying(255),
    modified_at timestamp with time zone,
    modified_by character varying(255),
    removed_at  timestamp with time zone,
    quantity    bigint             not null,
    book_id     bigint,
    foreign key (book_id) references public.books (id)
        match simple on update no action on delete no action
);
alter sequence public.book_purchase_history_id_seq owned by public.book_purchase_history.id;
-- endregion

-- region book_to_author
create sequence public.book_to_author_id_seq;
alter sequence public.book_to_author_id_seq owner to myuser;
create table public.book_to_author
(
    id          bigint primary key not null default nextval('book_to_author_id_seq'::regclass),
    active      boolean,
    created_at  timestamp with time zone,
    created_by  character varying(255),
    modified_at timestamp with time zone,
    modified_by character varying(255),
    removed_at  timestamp with time zone,
    author_id   bigint,
    book_id     bigint,
    foreign key (book_id) references public.books (id),
    foreign key (author_id) references public.authors (id)
);
alter sequence public.book_to_author_id_seq owned by public.book_to_author.id;
-- endregion

-- region user
create sequence public.users_id_seq;
alter sequence public.users_id_seq owner to myuser;
create table public.users
(
    id          bigint primary key     not null default nextval('users_id_seq'::regclass),
    active      boolean,
    created_at  timestamp with time zone,
    created_by  character varying(255),
    modified_at timestamp with time zone,
    modified_by character varying(255),
    removed_at  timestamp with time zone,
    email       character varying(255) not null,
    first_name  character varying(255),
    last_name   character varying(255),
    sso_id      character varying(255) not null,
    type        character varying(255) not null,
    username    character varying(255) not null
);
create unique index uk_6dotkott2kjsp8vw4d0m25fb7 on users using btree (email);
create unique index uk_r43af9ap4edm43mmtq01oddj6 on users using btree (username);
alter sequence public.users_id_seq owned by public.users.id;
-- endregion

-- region user_preferences
create sequence public.user_preferences_id_seq;
alter sequence public.user_preferences_id_seq owner to myuser;
create table public.user_preferences
(
    id          bigint primary key not null default nextval('user_preferences_id_seq'::regclass),
    active      boolean,
    created_at  timestamp with time zone,
    created_by  character varying(255),
    modified_at timestamp with time zone,
    modified_by character varying(255),
    removed_at  timestamp with time zone,
    user_id     bigint,
    foreign key (user_id) references public.users (id)
        match simple on update no action on delete no action
);
create unique index uk_qy8dkrkc8b34dcgwoq2km43rd on user_preferences using btree (user_id);
alter sequence public.user_preferences_id_seq owned by public.user_preferences.id;
-- endregion

-- region user_preferences_genres
create table public.user_preferences_genres
(
    user_preference_id bigint not null,
    genres_id          bigint not null,
    primary key (user_preference_id, genres_id),
    foreign key (genres_id) references public.genres (id)
        match simple on update no action on delete no action,
    foreign key (user_preference_id) references public.user_preferences (id)
        match simple on update no action on delete no action
);
-- endregion