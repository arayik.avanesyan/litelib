package com.litelib.handler;

import com.litelib.model.response.ApiResponse;
import com.litelib.service.exceptions.CsvParsingRuntimeException;
import com.litelib.service.exceptions.DuplicateEntityForConditionException;
import com.litelib.service.exceptions.OutOfStockException;
import com.litelib.service.exceptions.ServiceRuntimeException;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashSet;
import java.util.List;
import java.util.Locale;

@Log4j2
@RestControllerAdvice
public class RestErrorHandler {

    private final MessageSource messageSource;

    @Autowired
    public RestErrorHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public ResponseEntity<ApiResponse<List<ErrorInfoDto>>> processValidationError(final MethodArgumentNotValidException ex) {

        final List<ErrorInfoDto> fieldErrors = ex.getBindingResult().getFieldErrors()
                .stream()
                .map(fieldError -> new ErrorInfoDto(fieldError.getField() + ": " + resolveLocalizedErrorMessage(fieldError)))
                .toList();

        final ApiResponse<List<ErrorInfoDto>> response = new ApiResponse<>();
        response.setErrors(new HashSet<>(fieldErrors));
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IllegalStateException.class)
    @ResponseBody
    public ResponseEntity<ApiResponse<ErrorInfoDto>> handleIllegalStateException(final IllegalStateException ex) {
        return buildResponse(HttpStatus.BAD_REQUEST, ex);
    }

    @ExceptionHandler(OutOfStockException.class)
    @ResponseBody
    public ResponseEntity<ApiResponse<ErrorInfoDto>> handleOutOfStockException(final OutOfStockException ex) {
        return buildResponse(HttpStatus.LOCKED, ex);
    }

    @ExceptionHandler(CsvParsingRuntimeException.class)
    @ResponseBody
    public ResponseEntity<ApiResponse<ErrorInfoDto>> handleCsvParsingRuntimeException(final CsvParsingRuntimeException ex) {
        log.error("Error occurred while parsing CSV with cause: {}", ex.getCause().getMessage());
        return buildResponse(HttpStatus.UNPROCESSABLE_ENTITY, ex);
    }

    @ExceptionHandler(ServiceRuntimeException.class)
    @ResponseBody
    public ResponseEntity<ApiResponse<ErrorInfoDto>> handleServiceRuntimeException(final ServiceRuntimeException ex) {
        log.error("Error occurred while contacting to other service: {}", ex.getMessage());
        return buildResponse(HttpStatus.UNPROCESSABLE_ENTITY, ex);
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseBody
    public ResponseEntity<ApiResponse<ErrorInfoDto>> handleIllegalStateException(final AccessDeniedException ex) {
        return buildResponse(HttpStatus.FORBIDDEN, ex);
    }

    @ExceptionHandler(EntityExistsException.class)
    @ResponseBody
    public ResponseEntity<ApiResponse<ErrorInfoDto>> handleEntityExistsException(final EntityExistsException ex) {
        return buildResponse(HttpStatus.CONFLICT, ex);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseBody
    public ResponseEntity<ApiResponse<ErrorInfoDto>> handleEntityNotFoundException(final EntityNotFoundException ex) {
        return buildResponse(HttpStatus.NOT_FOUND, ex);
    }

    @ExceptionHandler(DuplicateEntityForConditionException.class)
    @ResponseBody
    public ResponseEntity<ApiResponse<ErrorInfoDto>> handleConflict(final DuplicateEntityForConditionException ex) {
        return buildResponse(HttpStatus.CONFLICT, ex);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseBody
    public ResponseEntity<ApiResponse<ErrorInfoDto>> handleIllegalArgumentException(final IllegalArgumentException ex) {
        return buildResponse(HttpStatus.BAD_REQUEST, ex);
    }

    private String resolveLocalizedErrorMessage(FieldError fieldError) {
        final Locale currentLocale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(fieldError, currentLocale);
    }

    private ResponseEntity<ApiResponse<ErrorInfoDto>> buildResponse(final HttpStatus status, final Exception ex) {
        log.error(ex.getMessage());
        final ApiResponse<ErrorInfoDto> errorApiResponse = new ApiResponse<>();
        errorApiResponse.getErrors().add(new ErrorInfoDto(ex.getMessage()));
        return new ResponseEntity<>(errorApiResponse, status);
    }
}
