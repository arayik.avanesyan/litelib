package com.litelib.model.external;

import lombok.Data;

import java.util.List;

@Data
public class BookExternalResponse {
    private String status;
    private String code;
    private Long total;
    private List<BookExternalModel> data;
}
