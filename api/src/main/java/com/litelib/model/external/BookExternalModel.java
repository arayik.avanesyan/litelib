package com.litelib.model.external;

import lombok.Data;

import java.time.LocalDate;

@Data
public class BookExternalModel {
    private String title;
    private String author;
    private String genre;
    private String description;
    private String isbn;
    private String image;
    private LocalDate published;
    private String publisher;
}
