package com.litelib.model.response;

import com.litelib.handler.ErrorInfoDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
public class ApiResponse<T> {

    private T data;

    private Set<ErrorInfoDto> errors = new HashSet<>();

    private Meta meta;

    public ApiResponse(T data) {
        this.data = data;
    }

    public ApiResponse(T data, Integer page, Integer size, Long count) {
        this.data = data;
        this.meta = new Meta(page, size, count);
    }

    @Data
    @AllArgsConstructor
    public static class Meta {
        private int page;
        private int size;
        private long totalElements;
    }
}
