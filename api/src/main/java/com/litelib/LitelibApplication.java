package com.litelib;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LitelibApplication {

    public static void main(String[] args) {
        SpringApplication.run(LitelibApplication.class, args);
    }

}
