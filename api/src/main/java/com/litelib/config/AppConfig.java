package com.litelib.config;

import com.litelib.external.BookExternalApi;
import com.litelib.external.BookExternalMapper;
import com.litelib.model.external.BookExternalModel;
import com.litelib.model.external.BookExternalResponse;
import com.litelib.service.book.BookService;
import com.litelib.service.book.dto.BookCreateDto;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Log4j2
@Configuration
public class AppConfig {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplateBuilder().build();
    }

    @ConditionalOnProperty(name = "books.fetch.enabled", havingValue = "true")
    @Bean
    ApplicationRunner appStarted(final BookExternalApi bookExternalApi,
                                 final BookService bookService,
                                 final BookExternalMapper bookExternalMapper) {
        return args -> {
            final BookExternalResponse booksResponse = bookExternalApi.getBooks();
            List<BookExternalModel> books = booksResponse.getData();

            log.info("Loaded books: {}", books.size());

            List<BookCreateDto> bookDtoList = bookExternalMapper.mapExternalModelListToCreateDtoList(books);
            bookService.batchCreate(bookDtoList);
        };
    }
}
