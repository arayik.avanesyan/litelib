package com.litelib.controller;

import com.litelib.model.response.ApiResponse;
import com.litelib.service.book.BookService;
import com.litelib.service.book.BookSuggestionService;
import com.litelib.service.book.dto.BookCreateDto;
import com.litelib.service.book.dto.BookDto;
import com.litelib.service.book.dto.BookPurchaseDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Log4j2
@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/books")
public class BookController {

    private final BookService bookService;

    private final BookSuggestionService bookSuggestionService;

    @PreAuthorize("hasAuthority('USER')")
    @GetMapping
    public ResponseEntity<ApiResponse<List<BookDto>>> getAll(@RequestParam(value = "page", defaultValue = "0", required = false) Integer page,
                                                             @RequestParam(value = "size", defaultValue = "1000", required = false) Integer size,
                                                             @RequestParam(value = "search", defaultValue = "", required = false) String search) {
        final List<BookDto> books = bookService.getAll(page, size, search);
        final var count = bookService.count(search);
        final ApiResponse<List<BookDto>> apiResponse = new ApiResponse<>(books, page, size, count);
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    // assume there is a pagination
    @PreAuthorize("hasAuthority('USER')")
    @GetMapping("/preferences")
    public ResponseEntity<ApiResponse<List<BookDto>>> getPreferences(@RequestParam(value = "page", defaultValue = "0", required = false) Integer page,
                                                                     @RequestParam(value = "size", defaultValue = "1000", required = false) Integer size,
                                                                     @RequestParam(value = "search", defaultValue = "", required = false) String search) {
        final List<BookDto> books = bookSuggestionService.suggestBooks(page, size, search);
        final var count = bookSuggestionService.getSuggestBooksCount(search);
        final ApiResponse<List<BookDto>> apiResponse = new ApiResponse<>(books, page, size, count);
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('USER')")
    @GetMapping("/{id}")
    public ResponseEntity<ApiResponse<BookDto>> get(@PathVariable final Long id) {
        final BookDto bookDto = bookService.get(id);
        final ApiResponse<BookDto> apiResponse = new ApiResponse<>(bookDto);
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('SUPER_ADMIN')")
    @PostMapping
    public ResponseEntity<ApiResponse<Long>> create(@RequestBody final BookCreateDto createDto) {
        final Long id = bookService.create(createDto);
        final ApiResponse<Long> apiResponse = new ApiResponse<>(id);
        return new ResponseEntity<>(apiResponse, HttpStatus.CREATED);
    }

    @PreAuthorize("hasAuthority('USER')")
    @PostMapping("/{id}/purchase")
    public ResponseEntity<ApiResponse<?>> purchases(@PathVariable final Long id, @RequestBody final BookPurchaseDto dto) {
        bookService.purchases(id, dto);
        final ApiResponse<Long> apiResponse = new ApiResponse<>();
        return new ResponseEntity<>(apiResponse, HttpStatus.CREATED);
    }

}
