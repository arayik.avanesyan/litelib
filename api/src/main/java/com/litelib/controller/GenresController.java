package com.litelib.controller;

import com.litelib.model.response.ApiResponse;
import com.litelib.service.genre.GenreService;
import com.litelib.service.genre.dto.GenreDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Log4j2
@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/genres")
public class GenresController {

    private final GenreService genreService;

    @GetMapping
    public ResponseEntity<ApiResponse<?>> getGenres() {
        List<GenreDto> genres = genreService.getAll();
        return new ResponseEntity<>(new ApiResponse<>(genres), HttpStatus.OK);
    }

}
