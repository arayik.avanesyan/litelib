package com.litelib.controller;

import com.litelib.model.response.ApiResponse;
import com.litelib.service.bookpurchasehistory.BookPurchaseHistoryService;
import com.litelib.service.bookpurchasehistory.dto.BookPurchaseHistoryDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Log4j2
@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/books/purchase")
public class BookPurchaseHistoryController {

    private final BookPurchaseHistoryService bookPurchaseHistoryService;

    @PreAuthorize("hasAuthority('SUPER_ADMIN')")
    @GetMapping("/history")
    public ResponseEntity<ApiResponse<List<BookPurchaseHistoryDto>>> getAll(@RequestParam(value = "page", defaultValue = "0", required = false) Integer page,
                                                                            @RequestParam(value = "size", defaultValue = "1000", required = false) Integer size,
                                                                            @RequestParam(value = "search", required = false) String search) {
        final List<BookPurchaseHistoryDto> books = bookPurchaseHistoryService.getALl();
        final ApiResponse<List<BookPurchaseHistoryDto>> apiResponse = new ApiResponse<>(books);
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

}