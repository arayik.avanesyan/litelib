package com.litelib.controller;

import com.litelib.model.response.ApiResponse;
import com.litelib.service.userpreference.UserPreferenceService;
import com.litelib.service.userpreference.dto.UserPreferenceDto;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/preferences")
public class UserPreferencesController {

    private final UserPreferenceService userPreferenceService;

    @PreAuthorize("hasAuthority('USER')")
    @PutMapping
    public ResponseEntity<ApiResponse<?>> updateUserPreferences(@Valid @RequestBody UserPreferenceDto preferenceDto) {
        userPreferenceService.updateOrCreateUserPreference(preferenceDto.getGenreIds());
        return new ResponseEntity<>(new ApiResponse<>(), HttpStatus.OK);
    }


}
