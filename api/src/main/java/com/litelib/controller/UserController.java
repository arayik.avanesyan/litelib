package com.litelib.controller;

import com.litelib.model.response.ApiResponse;
import com.litelib.persistence.user.UserRole;
import com.litelib.persistence.user.UserType;
import com.litelib.service.security.SecurityService;
import com.litelib.service.user.UserService;
import com.litelib.service.user.dto.UserCreateDto;
import com.litelib.service.user.dto.UserDto;
import com.litelib.service.user.dto.UserUpdateDto;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Log4j2
@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/users")
public class UserController {

    private final UserService userService;

    private final SecurityService securityService;

    @PreAuthorize("hasAnyAuthority('SUPER_ADMIN', 'ADMIN')")
    @PostMapping("/import")
    public ResponseEntity<ApiResponse<?>> importUsers(@RequestParam("file") final MultipartFile file) {
        userService.importUsers(file);
        return new ResponseEntity<>(new ApiResponse<>(), HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority('SUPER_ADMIN', 'ADMIN')")
    @GetMapping
    public ResponseEntity<ApiResponse<List<UserDto>>> getAll() {
        final List<UserDto> users = userService.getAll();
        final var apiResponse = new ApiResponse<>(users);
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('SUPER_ADMIN', 'ADMIN')")
    @PostMapping
    public ResponseEntity<ApiResponse<Long>> create(@Valid @RequestBody final UserCreateDto createDto) {
        // super admin can ADD ONLY admins and admin can MANAGE ONLY users
        final UserType type = securityService.hasRole(UserRole.ADMIN) ? UserType.USER : UserType.ADMIN;
        final Long id = userService.create(createDto, type, true);
        final var apiResponse = new ApiResponse<>(id);
        return new ResponseEntity<>(apiResponse, HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<ApiResponse<?>> update(@PathVariable final Long id,
                                                 @Valid @RequestBody final UserUpdateDto userUpdateDto) {
        userService.update(id, userUpdateDto);
        return new ResponseEntity<>(new ApiResponse<>(), HttpStatus.NOT_FOUND);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse<?>> delete(@PathVariable final Long id) {
        userService.delete(id);
        return new ResponseEntity<>(new ApiResponse<>(), HttpStatus.NO_CONTENT);
    }


}
