package com.litelib.external;

import com.litelib.model.external.BookExternalResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class BookExternalApi {

    @Value("${books.fetch.api}")
    private String booksFetchApi;

    @Autowired
    private RestTemplate restTemplate;

    public BookExternalResponse getBooks() {
        final ResponseEntity<BookExternalResponse> responseEntity = restTemplate
                .getForEntity(booksFetchApi, BookExternalResponse.class);
        return responseEntity.getBody();
    }

}
