package com.litelib.external;

import com.litelib.model.external.BookExternalModel;
import com.litelib.service.book.dto.BookCreateDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;

@Mapper(componentModel = "spring")
public interface BookExternalMapper {

    @Named("localDateToZonedDateTime")
    static ZonedDateTime localDateToZonedDateTime(LocalDate localDate) {
        if (localDate == null) {
            return null;
        }
        return localDate.atStartOfDay(ZoneOffset.UTC);
    }

    @Mapping(target = "published", source = "published", qualifiedByName = "localDateToZonedDateTime")
    BookCreateDto mapExternalModelToCreateDto(BookExternalModel book);

    List<BookCreateDto> mapExternalModelListToCreateDtoList(List<BookExternalModel> books);
}
