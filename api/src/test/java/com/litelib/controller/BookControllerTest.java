package com.litelib.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.litelib.controller.config.TestSecurityConfig;
import com.litelib.service.book.dto.BookCreateDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.AuditorAware;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.Optional;

import static java.time.ZoneOffset.UTC;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("it")
@Import(TestSecurityConfig.class)
class BookControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private AuditorAware<String> auditorAware;

    @WithMockUser(authorities = "SUPER_ADMIN")
    @Test
    void create() throws Exception {
        BookCreateDto bookCreateDto = createBookCreateDto();
        when(auditorAware.getCurrentAuditor())
                .thenReturn(Optional.of("testuser"));
        mvc.perform(post("/api/v1/books")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(bookCreateDto)))
                .andExpect(status().is(HttpStatus.CREATED.value()));
    }

    private BookCreateDto createBookCreateDto() {
        final BookCreateDto dto = new BookCreateDto();
        dto.setTitle("Bill!' then the.");
        dto.setAuthor("Destin Rolfson");
        dto.setGenre("Ipsum");
        dto.setDescription("She said the King said to herself, and nibbled a little startled by seeing the Cheshire Cat: now I shall have to whisper a hint to Time, and round Alice, every now and then added them up, and began.");
        dto.setIsbn("9793684412818");
        dto.setImage("https://placeimg.com/480/640/any");
        dto.setPublished(ZonedDateTime.of(LocalDate.of(1970, 10, 9), LocalTime.MIN, UTC));
        dto.setPublisher("Ipsum Repudiandae");
        return dto;
    }
}